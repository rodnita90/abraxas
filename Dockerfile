FROM ubuntu:16.04

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y \
	git \
	python \
	python-dev \
	python-setuptools \
	python-pip \
	nginx \
	supervisor \
	libmysqlclient-dev

RUN pip install gunicorn

RUN echo "daemon off;" >> /etc/nginx/nginx.conf
COPY nginx-app.conf /etc/nginx/sites-available/default
COPY supervisor-app.conf /etc/supervisor/conf.d/


COPY requirements.txt /home/docker/code/app/
RUN pip install -r /home/docker/code/app/requirements.txt

COPY . /home/docker/code/

EXPOSE 80
CMD ["supervisord", "-n"]