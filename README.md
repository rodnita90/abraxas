# Abraxas

### Pruebas

Existen pruebas para:

  - API (POST, DELETE, PATCH, GET)
  - El comando que se encarga de crear las 50 tareas random

Para correr las pruebas ejecutar el siguiente comando:

```
$ python manage.py test
```

### Creacion datos random

Para poder crear 50 tareas con datos random ejecutar el siguiente comando:

```
$ python manage.py create_random_data
```
