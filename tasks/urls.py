# -*- coding: utf-8 -*-

from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from tasks import views


urlpatterns = [
    url(
        r'^task/(?P<pk>[0-9]+)/$',
        views.TaskDetail.as_view(),
        name='task_detail'
    ),
    url(
        r'^task/',
        views.TasksList.as_view(),
        name='task_list'
    ),
]

urlpatterns = format_suffix_patterns(urlpatterns)