# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics
from tasks.filters import TaskFilter
from tasks.models import Task
from tasks.serializers import TaskSerializer


class TasksList(generics.ListCreateAPIView):
    """
    API for get a list of tasks
    """
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_class = TaskFilter


class TaskDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    API for get a list of tasks
    """
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
