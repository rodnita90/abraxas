# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import django_filters

from tasks.models import Task


class TaskFilter(django_filters.rest_framework.FilterSet):
    """Filters for Contacts."""

    q = django_filters.CharFilter(
        name="description",
        lookup_expr='icontains'
    )
    status = django_filters.CharFilter(
        name="status",
        lookup_expr='iexact'
    )

    class Meta:
        model = Task
        fields = ('status', )