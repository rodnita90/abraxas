from __future__ import absolute_import, unicode_literals

import random
import string

import datetime
from django.core.management.base import BaseCommand
from django.utils import timezone

from tasks.models import Task


class Command(BaseCommand):
    """
    Create random data.
    """

    def handle(self, *args, **options):
        for i in xrange(50):
            # random letters for description
            description = "".join(
                [random.choice(string.letters) for i in xrange(15)])

            # random duration
            duration = random.randint(1, 30)

            # registered time (80-100%) of duration
            percentage = random.randrange(80, 100)
            registered_time = duration * percentage / 100.0

            # Created in the last week
            end = timezone.now()
            start = timezone.now() - datetime.timedelta(days=7)
            created_on = start + datetime.timedelta(
                seconds=random.randint(0, int((end - start).total_seconds())),
            )

            Task.objects.create(
                created_on=created_on,
                description=description,
                duration=duration,
                registered_time=registered_time,
                status='COMPLETED',
            )
