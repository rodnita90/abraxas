# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils import timezone


class Task(models.Model):

    # Constants
    PENDING_STATUS = 'PENDING'
    COMPLETED_STATUS = 'COMPLETED'
    STATUS_CHOICES = (
        (PENDING_STATUS, 'Pendiente'),
        (COMPLETED_STATUS, 'Completado')
    )

    # Fields
    created_on = models.DateTimeField(
        'Creado el',
        default=timezone.now
    )
    description = models.TextField(
        'Descripción',
        default="",
        blank=True
    )
    duration = models.PositiveIntegerField(
        'Duración de la tarea en minutos'
    )
    registered_time = models.FloatField(
        'TIempo registrado en minutos',
        default=0
    )
    status = models.CharField(
        'Estatus',
        max_length=15,
        default=PENDING_STATUS,
        choices=STATUS_CHOICES
    )

    def __unicode__(self):
        return "{}-{}".format(self.status, self.duration)

    def __str__(self):
        return self.__unicode__()

