# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from tasks.models import Task


class TaskSerializer(serializers.ModelSerializer):
    """Serializes a Task"""

    class Meta:
        model = Task
        fields = "__all__"
        read_only_fields = ('created_on', 'completed_on')

    def validate(self, data):
        instance = self.instance

        if instance is not None and instance.status == Task.COMPLETED_STATUS:
            raise ValidationError('Cannot update a completed task')

        return data