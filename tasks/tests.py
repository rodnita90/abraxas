# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
from django.core import management
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone
from rest_framework.test import APIClient
from tasks.models import Task


class GetAllTasksTest(TestCase):

    def setUp(self):
        """Init data"""
        self.client = APIClient()
        Task.objects.create(
            description='primera tarea',
            status='PENDING',
            duration=10
        )
        Task.objects.create(
            description='segunda tarea',
            status='PENDING',
            duration=10
        )
        Task.objects.create(
            description='tercera tarea',
            status='COMPLETED',
            duration=10
        )

    def test_get_all_tasks(self):
        """Test for get all the tasks"""
        response = self.client.get(reverse('task_list'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 3)

    def test_filter_tasks_by_pending_status(self):
        """Test for get tasks filter by pending status"""
        response = self.client.get(
            reverse('task_list'),
            {'status': 'pending'}
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 2)

    def test_filter_tasks_by_completed_status(self):
        """Test for get tasks filter by completed status"""
        response = self.client.get(
            reverse('task_list'),
            {'status': 'completed'}
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)

    def test_filter_tasks_by_description(self):
        """Test for get tasks filter by description"""
        response = self.client.get(
            reverse('task_list'),
            {'q': 'primera'}
        )
        self.assertEqual(len(response.data), 1)

        response = self.client.get(
            reverse('task_list'),
            {'q': 'tarea'}
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 3)


class GetDetailTaskTest(TestCase):

    def setUp(self):
        """Init data"""
        self.client = APIClient()
        self.task = Task.objects.create(
            description='primera tarea',
            status='PENDING',
            duration=10,
            registered_time=10,
        )

    def test_exist_task(self):
        """Test exist task"""
        response = self.client.get(
            reverse('task_detail', args=(self.task.pk,))
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['id'], self.task.id)

    def test_not_exist_task(self):
        """Test not exist task"""
        response = self.client.get(
            reverse('task_detail', args=(100000,))
        )
        self.assertEqual(response.status_code, 404)

    def test_keys_in_response(self):
        """Test for validate the responses's keys"""
        response = self.client.get(
            reverse('task_detail', args=(self.task.pk,))
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['id'], self.task.id)
        self.assertEqual(response.data['description'], 'primera tarea')
        self.assertEqual(response.data['status'], 'PENDING')
        self.assertEqual(response.data['duration'], 10)
        self.assertEqual(response.data['registered_time'], 10)
        self.assertTrue('created_on' in response.data)


class DeleteTaskTest(TestCase):

    def setUp(self):
        """Init data"""
        self.client = APIClient()
        self.task = Task.objects.create(
            description='primera tarea',
            status='PENDING',
            duration=10,
            registered_time=10,
        )

    def test_delete_task(self):
        """Test delete task"""
        response = self.client.delete(
            reverse('task_detail', args=(self.task.pk,))
        )
        self.assertEqual(response.status_code, 204)
        self.assertEqual(0, Task.objects.count())

    def test_delete_not_exist_task(self):
        """Test delete, not exist task"""
        response = self.client.delete(
            reverse('task_detail', args=(100000,))
        )
        self.assertEqual(response.status_code, 404)
        self.assertEqual(1, Task.objects.count())


class UpdateTaskTest(TestCase):

    def setUp(self):
        """Init data"""
        self.client = APIClient()
        self.task_pending = Task.objects.create(
            description='primera tarea',
            status='PENDING',
            duration=10,
            registered_time=10,
        )
        self.task_completed = Task.objects.create(
            description='tarea completada',
            status='COMPLETED',
            duration=10,
            registered_time=10,
        )
        self.update_data = {
            'description': 'tarea actualizada',
            'duration': 20,
            'registered_time': 30
        }

    def test_update_task(self):
        """Test update task"""
        response = self.client.patch(
            reverse('task_detail', args=(self.task_pending.pk,)),
            data=self.update_data
        )
        self.assertEqual(response.status_code, 200)
        updated_task = Task.objects.get(pk=self.task_pending.pk)
        self.assertEqual(
            updated_task.description, self.update_data['description'])
        self.assertEqual(
            updated_task.duration, self.update_data['duration'])
        self.assertEqual(
            updated_task.registered_time, self.update_data['registered_time'])

    def test_update_not_exist_task(self):
        """Test update, not exist task"""
        response = self.client.patch(
            reverse('task_detail', args=(1000,)),
            data=self.update_data
        )
        self.assertEqual(response.status_code, 404)
        updated_task = Task.objects.get(pk=self.task_pending.pk)
        self.assertEqual(
            updated_task.description, 'primera tarea')
        self.assertEqual(
            updated_task.duration, 10)
        self.assertEqual(
            updated_task.registered_time, 10)

    def test_update_completed_task(self):
        """Test update, we cant update a copleted task"""
        response = self.client.patch(
            reverse('task_detail', args=(self.task_completed.pk,)),
            data=self.update_data
        )
        self.assertEqual(response.status_code, 400)
        updated_task = Task.objects.get(pk=self.task_completed.pk)
        self.assertEqual(
            updated_task.description, 'tarea completada')
        self.assertEqual(
            updated_task.duration, 10)
        self.assertEqual(
            updated_task.registered_time, 10)


class CreateTaskTest(TestCase):

    def setUp(self):
        """Init data"""
        self.client = APIClient()
        self.data = {
            'description': 'tarea creada',
            'duration': 20,
            'registered_time': 30
        }

    def test_crceate_task(self):
        """Test create task"""
        response = self.client.post(
            reverse('task_list'),
            data=self.data
        )
        self.assertEqual(response.status_code, 201)
        self.assertEqual(1, Task.objects.count())
        created_task = Task.objects.first()
        self.assertEqual(
            created_task.description, self.data['description'])
        self.assertEqual(
            created_task.duration, self.data['duration'])
        self.assertEqual(
            created_task.registered_time, self.data['registered_time'])


class CreateRandomDataTest(TestCase):

    def test_create_50_tasks(self):
        """Test for check if command create 50 tasks"""
        management.call_command('create_random_data')
        self.assertEqual(Task.objects.count(), 50)

    def test_create_task_in_last_week(self):
        """Test for check if all the created tasks
        have a date in the last week"""
        end = timezone.now()
        start = timezone.now() - datetime.timedelta(days=7)
        management.call_command('create_random_data')

        for task in Task.objects.all().iterator():
            self.assertTrue(start <= task.created_on <= end)

    def test_create_task_registered_time(self):
        """Test for check if all the created tasks
        have a registered time that consumes 80 to 100% of the duration
        of the task"""
        management.call_command('create_random_data')

        for task in Task.objects.all().iterator():
            self.assertTrue(
                80 <= (task.registered_time * 100 / task.duration) <= 100)
